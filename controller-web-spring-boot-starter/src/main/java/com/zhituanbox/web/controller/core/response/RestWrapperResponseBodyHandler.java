package com.zhituanbox.web.controller.core.response;

import org.springframework.lang.Nullable;

/**
 * 返回值包装处理器
 *
 * @author LiuYang
 * @since 2020/9/30
 * @version 1.0.0
 */
public interface RestWrapperResponseBodyHandler<R> {

    /**
     * 包装
     *
     * @param body body
     * @return 包装之后的值
     */
    R wrap(@Nullable Object body);


}
