package com.zhituanbox.web.controller.core.response;

import cn.hutool.cache.Cache;
import cn.hutool.cache.impl.LRUCache;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.exceptions.ExceptionUtil;
import cn.hutool.core.util.ClassUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.core.util.TypeUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zhituanbox.core.aop.AopUtils;
import com.zhituanbox.core.context.SpringApplicationContextUtil;
import com.zhituanbox.core.enums.IBaseEnum;
import com.zhituanbox.web.controller.config.RestWrapperResponseBodyProperties;
import com.zhituanbox.web.controller.core.response.annotation.UnWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * <p>Abstract AbstractRestWrapperResponseBodyAdvice class.</p>
 *
 * @author LiuYang
 * @version 1.0.0
 */
public abstract class AbstractRestWrapperResponseBodyAdvice implements ResponseBodyAdvice<Object> {
    private final Logger log = LoggerFactory.getLogger(AbstractRestWrapperResponseBodyAdvice.class);

    @Autowired
    @Lazy
    protected ObjectMapper objectMapper;
    @Lazy
    @Autowired
    protected RestWrapperResponseBodyHandler<?> wrapperHandler;

    protected final Function<Class<?>, Boolean> containsBean = clazz -> !SpringApplicationContextUtil.getApplicationContext().getBeansOfType(clazz).isEmpty();

    /**
     * RestWrapperResponseBodyHandler接口的泛型类型
     */
    private final Cache<Class, Type> genericTypeCache = new LRUCache<>(5, DateUnit.HOUR.getMillis());

    /** {@inheritDoc} */
    @Override
    public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
        if (!containsBean.apply(RestWrapperResponseBodyHandler.class)) {
            throw new NoSuchBeanDefinitionException(RestWrapperResponseBodyHandler.class);
        }

        if (returnType.hasMethodAnnotation(UnWrapper.class) || returnType.getContainingClass().isAnnotationPresent(UnWrapper.class)) {
            log.debug("UnWrapper");
            return false;
        }

        //只处理基本类型和string和bean对象和没有返回值的情况
        Predicate<Class<?>> supports = ClassUtil::isBasicType;
        supports = supports.or(Void.TYPE::equals);
        supports = supports.or(BeanUtil::isBean);
        supports = supports.or(returnTypeClass -> ClassUtil.isAssignable(CharSequence.class, returnTypeClass));
        supports = supports.or(returnTypeClass -> {
            log.debug("class '{}' {} converterType: {}", returnType.getDeclaringClass().getSimpleName(), returnType, converterType.getSimpleName());
            return StrUtil.containsAnyIgnoreCase(converterType.getSimpleName(), "json", "Jackson");
        });
        return Objects.nonNull(returnType.getMethod()) && supports.test(returnType.getMethod().getReturnType());
    }

    /** {@inheritDoc} */
    @Override
    public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType, Class<? extends HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest request, ServerHttpResponse response) {
        Class<?> wrapperHandlerClass = AopUtils.getTargetImplClass(wrapperHandler);
        Type type = genericTypeCache.get(wrapperHandlerClass, () -> getGenericType().orElse(Object.class));
        //body 如果属于接口的泛型类，则返回本身
        if (Objects.nonNull(body) && ClassUtil.isAssignable(TypeUtil.getClass(type), body.getClass())) {
            return body;
        }
        //包装body
        Object wrapBody = wrapperHandler.wrap(body);

        //如果返回值是String类型，转为String
        if (Objects.nonNull(body) && ClassUtil.isAssignable(CharSequence.class, body.getClass())) {
            try {
                wrapBody = objectMapper.writeValueAsString(wrapBody);
            } catch (JsonProcessingException e) {
                throw ExceptionUtil.wrapRuntime(e);
            }
        }
        return wrapBody;
    }

    /**
     * 获取RestWrapperResponseBodyHandler接口的泛型类型
     * @return 泛型R类型
     */
    private Optional<Type> getGenericType() {
        Class<?> wrapperHandlerClass = AopUtils.getTargetImplClass(wrapperHandler);
        Type genericInterface = wrapperHandlerClass;
        do {
            for (Type anInterface : TypeUtil.getClass(genericInterface).getGenericInterfaces()) {
                if (ClassUtil.isAssignable(RestWrapperResponseBodyHandler.class, TypeUtil.getClass(anInterface))) {
                    genericInterface = anInterface;
                }
                if (RestWrapperResponseBodyHandler.class.equals(TypeUtil.getClass(anInterface))) {
                    genericInterface = anInterface;
                    break;
                }
            }
        } while (!RestWrapperResponseBodyHandler.class.equals(TypeUtil.getClass(genericInterface)));
        if (genericInterface instanceof ParameterizedType) {
            Optional<Type> typeOptional = Optional.ofNullable(TypeUtil.getTypeArgument(genericInterface));
            typeOptional.ifPresent(genericType -> {
                log.debug("{} implements RestWrapperResponseBodyHandler<{}>", wrapperHandlerClass.getSimpleName(), TypeUtil.getClass(genericType).getSimpleName());
            });
            return typeOptional;
        }
        return Optional.empty();
    }
}
