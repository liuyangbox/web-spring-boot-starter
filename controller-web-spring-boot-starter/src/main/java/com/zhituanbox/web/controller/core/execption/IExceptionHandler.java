package com.zhituanbox.web.controller.core.execption;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.util.PrimitiveIterator;

/**
 * 异常处理器
 *
 * @param <T> 需要处理的异常
 * @param <R> 需要
 * @author LiuYang
 * @version 1.0.0
 */
public interface IExceptionHandler<T extends Exception, R> {
    /** Constant <code>ORDER_STEP=1000</code> */
    int ORDER_STEP = 1000;

    /**
     * <p>getExceptionClass.</p>
     *
     * @return a {@link java.lang.Class} object.
     */
    @NotNull
    Class<T> getExceptionClass();

    /**
     * 异常处理
     *
     * @return 返回值
     * @param request a {@link javax.servlet.http.HttpServletRequest} object.
     * @param response a {@link javax.servlet.http.HttpServletResponse} object.
     * @param exception a T object.
     */
    R processException(@NotNull HttpServletRequest request, @NotNull HttpServletResponse response, @NotNull T exception);
}
