package com.zhituanbox.web.controller.support.exception;

import cn.hutool.core.util.StrUtil;
import com.zhituanbox.web.controller.core.execption.AbstractExceptionHandler;
import com.zhituanbox.web.controller.support.response.ExceptionResponseData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * <p>MissingServletRequestPartExceptionHandler class.</p>
 *
 * @author LiuYang
 * @version 1.0.0
 */
@Order(Ordered.HIGHEST_PRECEDENCE + 50)
public class MissingServletRequestPartExceptionHandler extends AbstractExceptionHandler<MissingServletRequestPartException> {
    private final Logger log = LoggerFactory.getLogger(MissingServletRequestPartException.class);

    /** {@inheritDoc} */
    @Override
    public @NotNull Class<MissingServletRequestPartException> getExceptionClass() {
        return MissingServletRequestPartException.class;
    }

    /** {@inheritDoc} */
    @Override
    public ExceptionResponseData<?> processException(@NotNull HttpServletRequest request, @NotNull HttpServletResponse response, @NotNull MissingServletRequestPartException exception) {
        final String defaultMessage = StrUtil.format("请求文件不存在：{}", exception.getRequestPartName());
        ExceptionResponseData<List<String>> exceptionResponseData = processException(exception, exception.getClass().getName(), defaultMessage, exception.getRequestPartName());
        log(log, exception, exceptionResponseData);
        return exceptionResponseData;
    }
}
