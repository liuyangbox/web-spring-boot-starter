package com.zhituanbox.web.controller.config;

import com.zhituanbox.core.config.AbstractEnableProperties;
import com.zhituanbox.core.config.BoxCoreConstant;
import com.zhituanbox.web.controller.core.response.RestWrapperResponseBodyAdvice;
import com.zhituanbox.web.controller.core.response.RestWrapperResponseBodyHandler;

import java.util.List;

/**
 * 响应值参数配置
 *
 * @author LiuYang
 * @since 2020/9/30
 * @version 1.0.0
 */
public class RestWrapperResponseBodyProperties extends AbstractEnableProperties {
    /**
     * 返回值包装配置前缀
     */
    public static final String PROPERTIES_PREFIX = BoxCoreConstant.SPRING_CONFIGURATION_PROPERTIES_PREFIX + ".web.response.wrapper";

    /**
     * 需要排除的返回值类型
     * <p>当接口返回的是此类型时，不进行包装</p>
     * <p>该类与该类的子类都不会进行包装</p>
     * @see cn.hutool.core.util.ClassUtil#isAssignable(Class, Class)
     * @see RestWrapperResponseBodyHandler
     * @see RestWrapperResponseBodyAdvice
     */
    private Class<?>[] excludeClasses;

    /**
     * <p>Getter for the field <code>excludeClasses</code>.</p>
     *
     * @return an array of {@link java.lang.Class} objects.
     */
    public Class<?>[] getExcludeClasses() {
        return excludeClasses;
    }

    /**
     * <p>Setter for the field <code>excludeClasses</code>.</p>
     *
     * @param excludeClasses an array of {@link java.lang.Class} objects.
     */
    public void setExcludeClasses(Class<?>[] excludeClasses) {
        this.excludeClasses = excludeClasses;
    }
}
