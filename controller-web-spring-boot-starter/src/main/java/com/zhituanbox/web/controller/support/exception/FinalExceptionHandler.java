package com.zhituanbox.web.controller.support.exception;

import com.zhituanbox.web.controller.core.execption.AbstractExceptionHandler;
import com.zhituanbox.web.controller.core.execption.IExceptionHandler;
import com.zhituanbox.web.controller.support.response.ExceptionResponseData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;

/**
 * <p>FinalExceptionHandler class.</p>
 *
 * @author LiuYang
 * @version 1.0.0
 */
@Order(Ordered.LOWEST_PRECEDENCE - IExceptionHandler.ORDER_STEP)
public class FinalExceptionHandler extends AbstractExceptionHandler<Exception> {

    private final Logger log = LoggerFactory.getLogger(FinalExceptionHandler.class);

    /** {@inheritDoc} */
    @Override
    public @NotNull Class<Exception> getExceptionClass() {
        return Exception.class;
    }

    /** {@inheritDoc} */
    @Override
    public ExceptionResponseData<?> processException(@NotNull HttpServletRequest request, @NotNull HttpServletResponse response, @NotNull Exception exception) {
        ExceptionResponseData<?> exceptionResponseData = super.processException(request, response, exception);
        log(log, exception, exceptionResponseData);
        return exceptionResponseData;
    }
}
