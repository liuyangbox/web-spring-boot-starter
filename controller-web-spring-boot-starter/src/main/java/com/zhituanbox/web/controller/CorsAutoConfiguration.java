package com.zhituanbox.web.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.zhituanbox.core.config.BoxCoreConstant;
import com.zhituanbox.core.console.Console;
import com.zhituanbox.core.console.RowMapConsole;
import com.zhituanbox.core.lambda.util.LambdaUtils;
import com.zhituanbox.web.controller.config.CorsProperties;
import com.zhituanbox.web.controller.core.cors.CorsWebMvcConfigurer;
import com.zhituanbox.web.controller.core.cors.DynamicCorsFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.validation.constraints.NotNull;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

/**
 * 跨域配置
 *
 * @author LiuYang
 * @since 2020/9/29
 * @version 1.0.0
 */
@Configuration(proxyBeanMethods = false)
@EnableConfigurationProperties
@ConditionalOnProperty(prefix = CorsProperties.PROPERTIES_PREFIX, value = BoxCoreConstant.ENABLE_PROPERTIES_KEY, havingValue = "true", matchIfMissing = true)
public class CorsAutoConfiguration {

    private final Logger log = LoggerFactory.getLogger(CorsAutoConfiguration.class);

    /**
     * 跨域配置
     *
     * @return a {@link com.zhituanbox.web.controller.config.CorsProperties} object.
     */
    @Bean
    @ConfigurationProperties(prefix = CorsProperties.PROPERTIES_PREFIX)
    public CorsProperties corsProperties() {
        return new CorsProperties();
    }

    /**
     * 配置Bean
     *
     * @param corsProperties 配置
     * @return {@linkplain CorsWebMvcConfigurer}|{@linkplain }
     */
    @Bean
    @ConditionalOnExpression("${spring.zhituanbox.web.cors.dynamic:false}== false and ${spring.zhituanbox.web.cors.with-credentials:false}==false")
    public CorsWebMvcConfigurer corsWebMvcConfigurer(@NotNull CorsProperties corsProperties) {
        CorsWebMvcConfigurer bean = new CorsWebMvcConfigurer(corsProperties);
        logTable(corsProperties, bean.getClass());
        return bean;
    }

    /**
     * <p>dynamicCorsFilter.</p>
     *
     * @param corsProperties a {@link com.zhituanbox.web.controller.config.CorsProperties} object.
     * @return a {@link com.zhituanbox.web.controller.core.cors.DynamicCorsFilter} object.
     */
    @Bean
    @ConditionalOnExpression("${spring.zhituanbox.web.cors.dynamic:false} or ${spring.zhituanbox.web.cors.with-credentials:false}")
    public DynamicCorsFilter dynamicCorsFilter(@NotNull CorsProperties corsProperties) {
        DynamicCorsFilter bean = new DynamicCorsFilter(corsProperties);
        logTable(corsProperties, bean.getClass());
        return bean;
    }

    private void logTable(@NotNull CorsProperties corsProperties, Class clazz) {
        final String header = StrUtil.format("跨域: {}.class", clazz.getSimpleName());
        Map<String, String> items = new LinkedHashMap<>();
        Map<String, Object> bean = BeanUtil.beanToMap(corsProperties, false, true);
        bean.forEach((k, v) -> items.put(k, Convert.toStr(v)));
        if (Objects.nonNull(corsProperties.getMaxAge())) {
            items.put(LambdaUtils.resolveProperty(CorsProperties::getMaxAge), DateUtil.formatBetween(corsProperties.getMaxAge().toMillis()));
        }
        Console rowConsole = new RowMapConsole(header, items);
        rowConsole.lineHandler(log::info);
    }
}
