package com.zhituanbox.web.controller.support.response;

import cn.hutool.core.util.ClassUtil;
import com.zhituanbox.web.controller.core.response.RestWrapperResponseBodyHandler;

/**
 * <p>DefaultRestWrapperResponseBodyHandler class.</p>
 *
 * @author LiuYang
 * @since 2020/9/30
 * @version 1.0.0
 */
public class DefaultRestWrapperResponseBodyHandler implements RestWrapperResponseBodyHandler<ResponseData<?>> {

    /** {@inheritDoc} */
    @Override
    public ResponseData<?> wrap(Object body) {
        return ResponseData.success(body);
    }
}
