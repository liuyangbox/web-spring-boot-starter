package com.zhituanbox.web.controller.support.exception;

import cn.hutool.core.exceptions.ExceptionUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.StrUtil;
import com.zhituanbox.web.controller.core.execption.AbstractExceptionHandler;
import com.zhituanbox.web.controller.support.response.ExceptionResponseData;
import org.apache.tomcat.util.http.fileupload.impl.FileSizeLimitExceededException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Objects;

/**
 * <p>MaxUploadSizeExceededExceptionHandler class.</p>
 *
 * @author LiuYang
 * @version 1.0.0
 */
@Order(Ordered.HIGHEST_PRECEDENCE + 60)
public class MaxUploadSizeExceededExceptionHandler extends AbstractExceptionHandler<MaxUploadSizeExceededException> {
    private final Logger log = LoggerFactory.getLogger(MissingServletRequestPartException.class);

    /** {@inheritDoc} */
    @Override
    public @NotNull Class<MaxUploadSizeExceededException> getExceptionClass() {
        return MaxUploadSizeExceededException.class;
    }

    /** {@inheritDoc} */
    @Override
    public ExceptionResponseData<?> processException(@NotNull HttpServletRequest request, @NotNull HttpServletResponse response, @NotNull MaxUploadSizeExceededException exception) {
        final long maxUploadSize = getMaxUploadSize(exception);
        String readableFileSize = FileUtil.readableFileSize(maxUploadSize);
        final String defaultMessage = StrUtil.format("上传文件超过最大大小；允许最大上传：{}", readableFileSize);
        ExceptionResponseData<List<String>> exceptionResponseData = processException(exception, exception.getClass().getName(), defaultMessage, readableFileSize, maxUploadSize);
        log(log, exception, exceptionResponseData);
        return exceptionResponseData;
    }

    /**
     * 获取允许的最大上传大小
     * <code>有可能{@link MaxUploadSizeExceededException#getMaxUploadSize()}为0；而使用的父类message；父类可能包含{@link org.apache.tomcat.util.http.fileupload.impl.FileSizeLimitExceededException}</code>
     * <p>所以当{@link MaxUploadSizeExceededException#getMaxUploadSize()}为0；则会尝试获取{@link FileSizeLimitExceededException#getPermittedSize()}</p>
     * @param exception 异常
     * @return 大小字节数
     */
    @SuppressWarnings({"unchecked", "ConstantConditions"})
    private long getMaxUploadSize(@NotNull MaxUploadSizeExceededException exception) {
        FileSizeLimitExceededException fileSizeLimitExceededException = (FileSizeLimitExceededException) ExceptionUtil.getCausedBy(exception, FileSizeLimitExceededException.class);
        if (Objects.nonNull(fileSizeLimitExceededException)) {
            return fileSizeLimitExceededException.getPermittedSize();
        }
        return exception.getMaxUploadSize();
    }
}
