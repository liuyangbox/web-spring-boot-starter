package com.zhituanbox.web.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.StrUtil;
import com.zhituanbox.core.config.BoxCoreConstant;
import com.zhituanbox.core.console.Console;
import com.zhituanbox.core.console.RowMapConsole;
import com.zhituanbox.web.controller.config.ExceptionProperties;
import com.zhituanbox.web.controller.core.execption.ExceptionHandlerAdvice;
import com.zhituanbox.web.controller.support.exception.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.validation.constraints.NotNull;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 异常捕捉
 *
 * @author LiuYang
 * @since 2020/9/29
 * @version 1.0.0
 */

@Configuration(proxyBeanMethods = false)
@EnableConfigurationProperties
@ConditionalOnProperty(prefix = ExceptionProperties.PROPERTIES_PREFIX, value = BoxCoreConstant.ENABLE_PROPERTIES_KEY, havingValue = "true", matchIfMissing = true)
public class ExceptionAutoConfiguration {

    private final Logger log = LoggerFactory.getLogger(ExceptionAutoConfiguration.class);

    /**
     * 跨域配置
     *
     * @return a {@link com.zhituanbox.web.controller.config.ExceptionProperties} object.
     */
    @Bean
    @ConfigurationProperties(prefix = ExceptionProperties.PROPERTIES_PREFIX)
    public ExceptionProperties exceptionProperties() {
        return new ExceptionProperties();
    }


    /**
     * <p>exceptionHandlerAdvice.</p>
     *
     * @param exceptionProperties a {@link com.zhituanbox.web.controller.config.ExceptionProperties} object.
     * @return a {@link com.zhituanbox.web.controller.core.execption.ExceptionHandlerAdvice} object.
     */
    @Bean
    public ExceptionHandlerAdvice exceptionHandlerAdvice(ExceptionProperties exceptionProperties) {
        logTable(exceptionProperties, ExceptionHandlerAdvice.class);
        return new ExceptionHandlerAdvice();
    }

    /**
     * <p>finalExceptionHandler.</p>
     *
     * @return a {@link com.zhituanbox.web.controller.support.exception.FinalExceptionHandler} object.
     */
    @Bean
    public FinalExceptionHandler finalExceptionHandler() {
        return new FinalExceptionHandler();
    }

    /**
     * <p>businessExceptionExceptionHandler.</p>
     *
     * @return a {@link com.zhituanbox.web.controller.support.exception.BusinessExceptionExceptionHandler} object.
     */
    @Bean
    public BusinessExceptionExceptionHandler businessExceptionExceptionHandler() {
        return new BusinessExceptionExceptionHandler();
    }

    /**
     * <p>missingServletRequestParameterExceptionHandler.</p>
     *
     * @return a {@link com.zhituanbox.web.controller.support.exception.MissingServletRequestParameterExceptionHandler} object.
     */
    @Bean
    public MissingServletRequestParameterExceptionHandler missingServletRequestParameterExceptionHandler() {
        return new MissingServletRequestParameterExceptionHandler();
    }

    /**
     * <p>httpRequestMethodNotSupportedExceptionHandler.</p>
     *
     * @return a {@link com.zhituanbox.web.controller.support.exception.HttpRequestMethodNotSupportedExceptionHandler} object.
     */
    @Bean
    public HttpRequestMethodNotSupportedExceptionHandler httpRequestMethodNotSupportedExceptionHandler() {
        return new HttpRequestMethodNotSupportedExceptionHandler();
    }

    /**
     * <p>missingPathVariableExceptionHandler.</p>
     *
     * @return a {@link com.zhituanbox.web.controller.support.exception.MissingPathVariableExceptionHandler} object.
     */
    @Bean
    public MissingPathVariableExceptionHandler missingPathVariableExceptionHandler() {
        return new MissingPathVariableExceptionHandler();
    }

    /**
     * <p>missingServletRequestPartExceptionHandler.</p>
     *
     * @return a {@link com.zhituanbox.web.controller.support.exception.MissingServletRequestPartExceptionHandler} object.
     */
    @Bean
    public MissingServletRequestPartExceptionHandler missingServletRequestPartExceptionHandler() {
        return new MissingServletRequestPartExceptionHandler();
    }

    /**
     * <p>maxUploadSizeExceededExceptionHandler.</p>
     *
     * @return a {@link com.zhituanbox.web.controller.support.exception.MaxUploadSizeExceededExceptionHandler} object.
     */
    @Bean
    public MaxUploadSizeExceededExceptionHandler maxUploadSizeExceededExceptionHandler() {
        return new MaxUploadSizeExceededExceptionHandler();
    }

    private void logTable(@NotNull ExceptionProperties restWrapperResponseBodyProperties, Class<?> clazz) {
        final String header = StrUtil.format("全局异常处理: {}.class", clazz.getSimpleName());
        Map<String, String> items = new LinkedHashMap<>();
        Map<String, Object> bean = BeanUtil.beanToMap(restWrapperResponseBodyProperties, false, true);
        bean.forEach((k, v) -> items.put(k, Convert.toStr(v)));
        Console rowConsole = new RowMapConsole(header, items);
        rowConsole.lineHandler(log::info);
    }

}
