package com.zhituanbox.web.controller.core.cors;

import cn.hutool.core.util.ArrayUtil;
import com.zhituanbox.web.controller.config.CorsProperties;
import org.springframework.http.HttpMethod;
import org.springframework.web.servlet.config.annotation.CorsRegistration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.validation.constraints.NotNull;
import java.time.Duration;
import java.util.Optional;

/**
 * 跨域配置
 *
 * @author LiuYang
 * @since 2020/9/29
 * @version 1.0.0
 */
public class CorsWebMvcConfigurer implements WebMvcConfigurer {

    @NotNull
    private CorsProperties corsProperties;

    /**
     * <p>Constructor for CorsWebMvcConfigurer.</p>
     *
     * @param corsProperties a {@link com.zhituanbox.web.controller.config.CorsProperties} object.
     */
    public CorsWebMvcConfigurer(CorsProperties corsProperties) {
        this.corsProperties = corsProperties;
    }

    /** {@inheritDoc} */
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        CorsRegistration corsRegistration = registry.addMapping("/**");
        corsProperties.configureCorsRegistration(corsRegistration);
    }

}
