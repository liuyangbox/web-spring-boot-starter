package com.zhituanbox.web.controller.support.exception;

import com.zhituanbox.web.controller.core.execption.AbstractExceptionHandler;
import com.zhituanbox.web.controller.support.response.ExceptionResponseData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.web.bind.MissingServletRequestParameterException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * <p>MissingServletRequestParameterExceptionHandler class.</p>
 *
 * @author LiuYang
 * @version 1.0.0
 */
@Order(Ordered.HIGHEST_PRECEDENCE + 20)
public class MissingServletRequestParameterExceptionHandler extends AbstractExceptionHandler<MissingServletRequestParameterException> {
    private final Logger log = LoggerFactory.getLogger(MissingServletRequestParameterExceptionHandler.class);

    /** {@inheritDoc} */
    @Override
    public @NotNull Class<MissingServletRequestParameterException> getExceptionClass() {
        return MissingServletRequestParameterException.class;
    }

    /** {@inheritDoc} */
    @Override
    public ExceptionResponseData<?> processException(@NotNull HttpServletRequest request, @NotNull HttpServletResponse response, @NotNull MissingServletRequestParameterException exception) {
        ExceptionResponseData<List<String>> exceptionResponseData = processException(exception, exception.getClass().getName(), "缺少参数", exception.getParameterName());
        log(log, exception, exceptionResponseData);
        return exceptionResponseData;
    }
}
