package com.zhituanbox.web.controller.support.response;

/**
 * <p>ResponseData class.</p>
 *
 * @author LiuYang
 * @version 1.0.0
 */
public class ResponseData<T> {
    /**
     * 响应编码
     */
    private String code;
    /**
     * 消息
     */
    private String message;
    /**
     * 数据
     */
    private T data;

    /**
     * <p>success.</p>
     *
     * @param data a T object.
     * @param <T> a T object.
     * @return a {@link com.zhituanbox.web.controller.support.response.ResponseData} object.
     */
    public static <T> ResponseData<T> success(T data) {
        return new ResponseData<>("success", null, data);
    }

    /**
     * <p>Constructor for ResponseData.</p>
     */
    public ResponseData() {
    }

    /**
     * <p>Constructor for ResponseData.</p>
     *
     * @param code a {@link java.lang.String} object.
     * @param message a {@link java.lang.String} object.
     * @param data a T object.
     */
    public ResponseData(String code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    /**
     * <p>Getter for the field <code>code</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getCode() {
        return code;
    }

    /**
     * <p>Setter for the field <code>code</code>.</p>
     *
     * @param code a {@link java.lang.String} object.
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * <p>Getter for the field <code>message</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getMessage() {
        return message;
    }

    /**
     * <p>Setter for the field <code>message</code>.</p>
     *
     * @param message a {@link java.lang.String} object.
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * <p>Getter for the field <code>data</code>.</p>
     *
     * @return a T object.
     */
    public T getData() {
        return data;
    }

    /**
     * <p>Setter for the field <code>data</code>.</p>
     *
     * @param data a T object.
     */
    public void setData(T data) {
        this.data = data;
    }
}
