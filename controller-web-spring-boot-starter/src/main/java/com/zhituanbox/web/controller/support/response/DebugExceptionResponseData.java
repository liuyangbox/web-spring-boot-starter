package com.zhituanbox.web.controller.support.response;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;

import javax.validation.constraints.NotNull;

/**
 * 通用异常响应-debug模式
 *
 * @author LiuYang
 * @since  2020/5/28
 * @version 1.0.0
 */
public class DebugExceptionResponseData<T> extends ExceptionResponseData<T> {
    /**
     * 错误异常日志
     */
    private String exceptionLog;

    /**
     * <p>Constructor for DebugExceptionResponseData.</p>
     */
    public DebugExceptionResponseData() {
    }

    /**
     * <p>Constructor for DebugExceptionResponseData.</p>
     *
     * @param exceptionResponseData a {@link com.zhituanbox.web.controller.support.response.ExceptionResponseData} object.
     * @param exceptionLog a {@link java.lang.String} object.
     */
    public DebugExceptionResponseData(@NotNull ExceptionResponseData<T> exceptionResponseData, String exceptionLog) {
        BeanUtil.copyProperties(exceptionResponseData, this, new CopyOptions().setEditable(ExceptionResponseData.class));
        this.exceptionLog = exceptionLog;
    }

    /**
     * <p>Getter for the field <code>exceptionLog</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getExceptionLog() {
        return exceptionLog;
    }

    /**
     * <p>Setter for the field <code>exceptionLog</code>.</p>
     *
     * @param exceptionLog a {@link java.lang.String} object.
     */
    public void setExceptionLog(String exceptionLog) {
        this.exceptionLog = exceptionLog;
    }
}
