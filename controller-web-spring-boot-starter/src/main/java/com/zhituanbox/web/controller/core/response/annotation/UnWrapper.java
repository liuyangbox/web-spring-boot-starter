package com.zhituanbox.web.controller.core.response.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * 不包装
 * <p>返回原始值</p>
 *
 * @author LiuYang
 * @since 2020/9/30
 * @version 1.0.0
 */
@Target({METHOD, TYPE})
@Retention(RUNTIME)
@Documented
public @interface UnWrapper {
}
