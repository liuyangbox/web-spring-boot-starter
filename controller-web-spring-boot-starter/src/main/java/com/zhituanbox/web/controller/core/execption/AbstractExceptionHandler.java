package com.zhituanbox.web.controller.core.execption;

import com.zhituanbox.web.controller.config.ExceptionProperties;
import com.zhituanbox.web.controller.support.response.DebugExceptionResponseData;
import com.zhituanbox.web.controller.support.response.ExceptionResponseData;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.lang.Nullable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

/**
 * <p>Abstract AbstractExceptionHandler class.</p>
 *
 * @author LiuYang
 * @version 1.0.0
 */
public abstract class AbstractExceptionHandler<T extends Exception> implements IExceptionHandler<T, ExceptionResponseData<?>> {
    @Autowired
    private MessageSource messageSource;
    @Autowired
    protected ExceptionProperties exceptionProperties;
    private static MessageSource digestMessageSource;

    /** {@inheritDoc} */
    @Override
    public ExceptionResponseData<?> processException(@NotNull HttpServletRequest request, @NotNull HttpServletResponse response, @NotNull T exception) {
        return processException(exception, exception.getClass().getName(), "服务异常");
    }

    /**
     * <p>processException.</p>
     *
     * @param exception a {@link java.lang.Exception} object.
     * @param code a {@link java.lang.String} object.
     * @param defaultMessage a {@link java.lang.String} object.
     * @param codeArgs a {@link java.lang.Object} object.
     * @return a {@link com.zhituanbox.web.controller.support.response.ExceptionResponseData} object.
     */
    protected ExceptionResponseData<List<String>> processException(@NotNull Exception exception, @NotBlank String code, @Nullable String defaultMessage, Object... codeArgs) {
        ExceptionResponseData<List<String>> exceptionResponseData = new ExceptionResponseData<>();
        exceptionResponseData.setBusinessException(false);
        exceptionResponseData.setCode(code);
        String message = getMessage(exceptionResponseData.getCode(), codeArgs, defaultMessage, LocaleContextHolder.getLocale());
        exceptionResponseData.setMessage(message);

        //堆栈跟踪模式开启
        if (exceptionProperties.isDebug()) {
            exceptionResponseData = new DebugExceptionResponseData<>(exceptionResponseData, exception.getMessage());
            List<String> stackTrace = Arrays.stream(exception.getStackTrace()).map(StackTraceElement::toString).collect(Collectors.toList());
            exceptionResponseData.setData(stackTrace);
        }
        return exceptionResponseData;
    }

    /**
     * <p>log.</p>
     *
     * @param log a {@link org.slf4j.Logger} object.
     * @param exception a {@link java.lang.Exception} object.
     * @param exceptionResponseData a {@link com.zhituanbox.web.controller.support.response.ExceptionResponseData} object.
     */
    protected void log(final Logger log, Exception exception, ExceptionResponseData<?> exceptionResponseData) {
        if (exceptionResponseData.isBusinessException()) {
            log.warn("code: {}; message: {}", exceptionResponseData.getCode(), exceptionResponseData.getMessage(), exception);
        } else {
            log.error("code: {}; message: {}", exceptionResponseData.getCode(), exceptionResponseData.getMessage(), exception);
        }
    }

    private synchronized MessageSource getMessageSource() {
        if (digestMessageSource == null) {
            String sourceBundle = "messages-controller";

            ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
            messageSource.setBasenames("classpath:" + sourceBundle, sourceBundle);
            messageSource.setDefaultEncoding(StandardCharsets.UTF_8.name());
            digestMessageSource = messageSource;
        }
        return digestMessageSource;
    }

    /**
     * <p>getMessage.</p>
     *
     * @param code a {@link java.lang.String} object.
     * @param args an array of {@link java.lang.Object} objects.
     * @param defaultMessage a {@link java.lang.String} object.
     * @param locale a {@link java.util.Locale} object.
     * @return a {@link java.lang.String} object.
     */
    protected String getMessage(String code, @Nullable Object[] args, @Nullable String defaultMessage, Locale locale) {
        return messageSource.getMessage(code, args, getMessageSource().getMessage(code, args, defaultMessage, locale), locale);
    }
}
