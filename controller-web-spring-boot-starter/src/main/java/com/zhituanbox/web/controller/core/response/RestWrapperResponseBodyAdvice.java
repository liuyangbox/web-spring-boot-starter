package com.zhituanbox.web.controller.core.response;

import cn.hutool.cache.Cache;
import cn.hutool.cache.impl.LRUCache;
import cn.hutool.cache.impl.TimedCache;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.exceptions.ExceptionUtil;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.ClassUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.core.util.TypeUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zhituanbox.core.aop.AopUtils;
import com.zhituanbox.core.config.BoxCoreConstant;
import com.zhituanbox.core.context.SpringApplicationContextUtil;
import com.zhituanbox.core.lambda.util.LambdaUtils;
import com.zhituanbox.web.controller.config.RestWrapperResponseBodyProperties;
import com.zhituanbox.web.controller.core.response.annotation.UnWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * Rest接口值返回包装
 * <p>将返回值进行统一包装</p>
 *
 * @author LiuYang
 * @since 2020/9/30
 * @version 1.0.0
 */
@RestControllerAdvice
@ConditionalOnProperty(prefix = RestWrapperResponseBodyProperties.PROPERTIES_PREFIX, value = BoxCoreConstant.ENABLE_PROPERTIES_KEY, havingValue = "true")
public class RestWrapperResponseBodyAdvice extends AbstractRestWrapperResponseBodyAdvice implements ResponseBodyAdvice<Object> {

    private final Logger log = LoggerFactory.getLogger(RestWrapperResponseBodyAdvice.class);

    @Lazy
    @Autowired
    private RestWrapperResponseBodyProperties restWrapperResponseBodyProperties;

    private final Cache<String, Boolean> logCache = new TimedCache<>(DateUnit.DAY.getMillis());

    /** {@inheritDoc} */
    @Override
    public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {

        if (!containsBean.apply(RestWrapperResponseBodyProperties.class) || !restWrapperResponseBodyProperties.getEnable()) {
            logCache.get(LambdaUtils.resolveProperty(RestWrapperResponseBodyProperties::getEnable), () -> {
                log.debug("rest wrapper response body is disabled");
                return true;
            });
            return false;
        }
        return super.supports(returnType, converterType);
    }

    /** {@inheritDoc} */
    @Override
    public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType, Class<? extends HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest request, ServerHttpResponse response) {

        if (Objects.nonNull(body)
                && containsBean.apply(RestWrapperResponseBodyProperties.class)
                && ArrayUtil.isNotEmpty(restWrapperResponseBodyProperties.getExcludeClasses())) {
            for (Class<?> excludeClass : restWrapperResponseBodyProperties.getExcludeClasses()) {
                if (ClassUtil.isAssignable(excludeClass, body.getClass())) {
                    log.debug("returnType '{}' assignable for excludeClass '{}'", body.getClass().getName(), excludeClass.getName());
                    return true;
                }
            }
        }
        return super.beforeBodyWrite(body, returnType, selectedContentType, selectedConverterType, request, response);
    }
}
