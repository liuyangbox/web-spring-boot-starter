package com.zhituanbox.web.controller.support.exception;

import cn.hutool.core.util.StrUtil;
import com.zhituanbox.web.controller.core.execption.AbstractExceptionHandler;
import com.zhituanbox.web.controller.support.response.ExceptionResponseData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.web.HttpRequestMethodNotSupportedException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * <p>HttpRequestMethodNotSupportedExceptionHandler class.</p>
 *
 * @author LiuYang
 * @version 1.0.0
 */
@Order(Ordered.HIGHEST_PRECEDENCE + 30)
public class HttpRequestMethodNotSupportedExceptionHandler extends AbstractExceptionHandler<HttpRequestMethodNotSupportedException> {
    private final Logger log = LoggerFactory.getLogger(HttpRequestMethodNotSupportedExceptionHandler.class);

    /** {@inheritDoc} */
    @Override
    public @NotNull Class<HttpRequestMethodNotSupportedException> getExceptionClass() {
        return HttpRequestMethodNotSupportedException.class;
    }

    /** {@inheritDoc} */
    @Override
    public ExceptionResponseData<?> processException(@NotNull HttpServletRequest request, @NotNull HttpServletResponse response, @NotNull HttpRequestMethodNotSupportedException exception) {
        final String defaultMessage = StrUtil.format("请求方法{}不支持", exception.getMethod());
        ExceptionResponseData<List<String>> exceptionResponseData = processException(exception, exception.getClass().getName(), defaultMessage, exception.getMethod());
        log(log, exception, exceptionResponseData);
        return exceptionResponseData;
    }
}
