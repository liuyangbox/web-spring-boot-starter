package com.zhituanbox.web.controller.config;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ArrayUtil;
import com.zhituanbox.core.config.AbstractEnableProperties;
import com.zhituanbox.core.config.BoxCoreConstant;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.lang.Nullable;
import org.springframework.web.servlet.config.annotation.CorsRegistration;

import javax.validation.constraints.NotNull;
import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 跨域配置
 *
 * @author LiuYang
 * @since 2020/9/29
 * @version 1.0.0
 */
public class CorsProperties extends AbstractEnableProperties {
    /**
     * 跨域配置前缀
     */
    public static final String PROPERTIES_PREFIX = BoxCoreConstant.SPRING_CONFIGURATION_PROPERTIES_PREFIX + ".web.cors";

    /**
     * 是否动态响应
     * <p>当CorsProperties#withCredentials设置为true时，也会采用动态模式</p>
     * <p>当允许的时候，显示的参数为客户端传过来的值；解决火狐中设置*跨域失败问题</p>
     */
    private Boolean dynamic = Boolean.FALSE;

    /**
     * 缓存时间：默认30分钟
     */
    @Nullable
    private Duration maxAge;

    /**
     * 表示是否允许发送Cookie。默认情况下，Cookie不包括在CORS请求之中。设为true，即表示服务器明确许可，Cookie可以包含在请求中，一起发给服务器
     * @see HttpHeaders#ACCESS_CONTROL_ALLOW_CREDENTIALS
     */
    @NotNull
    private Boolean withCredentials = Boolean.FALSE;

    /**
     * 默认支持所有方法
     */
    @NotNull
    private Set<HttpMethod> allowedMethods = Arrays.stream(HttpMethod.values()).collect(Collectors.toSet());
    /**
     * 允许的源，默认所有
     */
    @NotNull
    private List<String> allowedOrigins = CollUtil.newArrayList("*");

    /**
     * 允许的请求头
     */
    @NotNull
    private List<String> allowedHeaders = CollUtil.newArrayList("*");

    /**
     * <p>Getter for the field <code>dynamic</code>.</p>
     *
     * @return a {@link java.lang.Boolean} object.
     */
    public Boolean getDynamic() {
        return dynamic;
    }

    /**
     * <p>Setter for the field <code>dynamic</code>.</p>
     *
     * @param dynamic a {@link java.lang.Boolean} object.
     */
    public void setDynamic(Boolean dynamic) {
        this.dynamic = dynamic;
    }

    /**
     * <p>Getter for the field <code>maxAge</code>.</p>
     *
     * @return a {@link java.time.Duration} object.
     */
    public Duration getMaxAge() {
        return maxAge;
    }

    /**
     * <p>Setter for the field <code>maxAge</code>.</p>
     *
     * @param maxAge a {@link java.time.Duration} object.
     */
    public void setMaxAge(Duration maxAge) {
        this.maxAge = maxAge;
    }

    /**
     * <p>Getter for the field <code>allowedMethods</code>.</p>
     *
     * @return a {@link java.util.Set} object.
     */
    public Set<HttpMethod> getAllowedMethods() {
        return allowedMethods;
    }

    /**
     * <p>Setter for the field <code>allowedMethods</code>.</p>
     *
     * @param allowedMethods a {@link java.util.Set} object.
     */
    public void setAllowedMethods(Set<HttpMethod> allowedMethods) {
        this.allowedMethods = allowedMethods;
    }

    /**
     * <p>Getter for the field <code>allowedOrigins</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<String> getAllowedOrigins() {
        return allowedOrigins;
    }

    /**
     * <p>Setter for the field <code>allowedOrigins</code>.</p>
     *
     * @param allowedOrigins a {@link java.util.List} object.
     */
    public void setAllowedOrigins(List<String> allowedOrigins) {
        this.allowedOrigins = allowedOrigins;
    }

    /**
     * <p>Getter for the field <code>withCredentials</code>.</p>
     *
     * @return a {@link java.lang.Boolean} object.
     */
    public Boolean getWithCredentials() {
        return withCredentials;
    }

    /**
     * <p>Setter for the field <code>withCredentials</code>.</p>
     *
     * @param withCredentials a {@link java.lang.Boolean} object.
     */
    public void setWithCredentials(Boolean withCredentials) {
        this.withCredentials = withCredentials;
    }

    /**
     * <p>Getter for the field <code>allowedHeaders</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<String> getAllowedHeaders() {
        return allowedHeaders;
    }

    /**
     * <p>Setter for the field <code>allowedHeaders</code>.</p>
     *
     * @param allowedHeaders a {@link java.util.List} object.
     */
    public void setAllowedHeaders(List<String> allowedHeaders) {
        this.allowedHeaders = allowedHeaders;
    }

    /**
     * <p>configureCorsRegistration.</p>
     *
     * @param corsRegistration a {@link org.springframework.web.servlet.config.annotation.CorsRegistration} object.
     * @return a {@link org.springframework.web.servlet.config.annotation.CorsRegistration} object.
     */
    @SuppressWarnings("UnusedReturnValue")
    public CorsRegistration configureCorsRegistration(@NotNull CorsRegistration corsRegistration) {
//        CorsRegistration corsRegistration = registry.addMapping("/**");
        CorsProperties corsProperties = this;
        ArrayUtil.toArray(corsProperties.getAllowedHeaders(), String.class);
        //设置请求源
        corsRegistration.allowedOrigins(corsProperties.getAllowedOrigins().toArray(new String[]{}));
        //设置请求方法
        String[] methods = corsProperties.getAllowedMethods().stream().map(HttpMethod::name).toArray(String[]::new);
        corsRegistration.allowedMethods(methods);
        //允许头
        corsRegistration.allowedHeaders(corsProperties.getAllowedHeaders().toArray(new String[]{}));
        //允许传输cookie
        corsRegistration.allowCredentials(corsProperties.getWithCredentials());
        //设置缓存时长
        Optional.ofNullable(corsProperties.getMaxAge()).map(Duration::getSeconds).ifPresent(corsRegistration::maxAge);
        return corsRegistration;
    }
}
