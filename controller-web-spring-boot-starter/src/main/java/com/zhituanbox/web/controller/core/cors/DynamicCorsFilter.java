package com.zhituanbox.web.controller.core.cors;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.zhituanbox.web.controller.config.CorsProperties;
import org.springframework.http.HttpHeaders;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsUtils;
import org.springframework.web.filter.CorsFilter;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;
import java.util.Set;

/**
 * <p>DynamicCorsFilter class.</p>
 *
 * @author LiuYang
 * @since 2020/9/30
 * @version 1.0.0
 */
public class DynamicCorsFilter extends CorsFilter {

    /**
     * <p>Constructor for DynamicCorsFilter.</p>
     *
     * @param corsProperties a {@link com.zhituanbox.web.controller.config.CorsProperties} object.
     */
    public DynamicCorsFilter(@NotNull CorsProperties corsProperties) {
        super(request -> {
            final String all = "*";
            CorsConfiguration corsConfiguration = new CorsConfiguration();
            corsConfiguration.setAllowCredentials(corsProperties.getWithCredentials());
            corsConfiguration.setAllowedHeaders(corsProperties.getAllowedHeaders());

            if (corsProperties.getAllowedMethods().stream().anyMatch(t -> t.matches(request.getMethod()))) {
                Set<String> allowedMethods = CollUtil.newHashSet(request.getMethod());
                if (CorsUtils.isPreFlightRequest(request)) {
                    //如果是跨域，则需要将原始请求添加进去
                    allowedMethods.add(request.getHeader(HttpHeaders.ACCESS_CONTROL_REQUEST_METHOD));
                }
                corsConfiguration.setAllowedMethods(CollUtil.newArrayList(allowedMethods));
            }

            HttpHeaders header = getHeader(request);
            if (corsProperties.getAllowedOrigins().contains(all)
                    || (StrUtil.isNotBlank(header.getOrigin())
                    && corsProperties.getAllowedOrigins().contains(header.getOrigin()))) {
                corsConfiguration.setAllowedOrigins(CollUtil.newArrayList(header.getOrigin()));
            } else {
                corsConfiguration.setAllowedOrigins(CollUtil.newArrayList(corsProperties.getAllowedOrigins()));
            }

            return corsConfiguration;
        });
    }

    private static HttpHeaders getHeader(HttpServletRequest request) {
        HttpHeaders httpHeaders = new HttpHeaders();
        for (String header : CollUtil.newArrayList(request.getHeaderNames())) {
            httpHeaders.addAll(header, CollUtil.newArrayList(request.getHeaders(header)));
        }
        return httpHeaders;
    }
}
