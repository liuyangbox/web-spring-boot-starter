package com.zhituanbox.web.controller.config;

import com.zhituanbox.core.config.AbstractEnableProperties;
import com.zhituanbox.core.config.BoxCoreConstant;

/**
 * <p>ExceptionProperties class.</p>
 *
 * @author LiuYang
 * @version 1.0.0
 */
public class ExceptionProperties extends AbstractEnableProperties {
    /**
     * 异常配置前缀
     */
    public static final String PROPERTIES_PREFIX = BoxCoreConstant.SPRING_CONFIGURATION_PROPERTIES_PREFIX + ".web.exception";

    /**
     * debug模式
     * <ul>
     *     <li>接口异常返回数据中返回exceptionLog</li>
     *     <li>接口异常返回数据中返回异常的StackTrace</li>
     * </ul>
     */
    private boolean debug = true;

    /**
     * <p>isDebug.</p>
     *
     * @return a boolean.
     */
    public boolean isDebug() {
        return debug;
    }

    /**
     * <p>Setter for the field <code>debug</code>.</p>
     *
     * @param debug a boolean.
     */
    public void setDebug(boolean debug) {
        this.debug = debug;
    }
}
