package com.zhituanbox.web.controller.support.response;

/**
 * 通用异常响应
 *
 * @author LiuYang
 * @since  2020/5/28
 * @version 1.0.0
 */
public class ExceptionResponseData<T> extends ResponseData<T> {
    /**
     * 是否业务异常
     */
    private boolean businessException = false;

    /**
     * <p>isBusinessException.</p>
     *
     * @return a boolean.
     */
    public boolean isBusinessException() {
        return businessException;
    }

    /**
     * <p>Setter for the field <code>businessException</code>.</p>
     *
     * @param businessException a boolean.
     */
    public void setBusinessException(boolean businessException) {
        this.businessException = businessException;
    }
}
