package com.zhituanbox.web.controller.core.execption;


import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ClassUtil;
import com.zhituanbox.core.aop.AopUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestControllerAdvice
/**
 * <p>ExceptionHandlerAdvice class.</p>
 *
 * @author LiuYang
 * @version 1.0.0
 */
public class ExceptionHandlerAdvice {

    private final Logger log = LoggerFactory.getLogger(ExceptionHandlerAdvice.class);

    @Autowired
    private ApplicationContext applicationContext;

//    private Cache<>

    /**
     * 其他所有的异常
     *
     * @param request a {@link javax.servlet.http.HttpServletRequest} object.
     * @param response a {@link javax.servlet.http.HttpServletResponse} object.
     * @param ex a {@link java.lang.Exception} object.
     * @return a {@link java.lang.Object} object.
     */
    @SuppressWarnings("rawtypes")
    @ExceptionHandler(Exception.class)
    public Object processException(HttpServletRequest request, HttpServletResponse response, Exception ex) {
        IExceptionHandler<Exception, ?> bestMatchExceptionHandler = matchBestExceptionHandler(ex).orElseThrow(() -> new NoSuchBeanDefinitionException(IExceptionHandler.class.getSimpleName()));
        return bestMatchExceptionHandler.processException(request, response, ex);
    }

    /**
     * <p>matchBestExceptionHandler.</p>
     *
     * @param ex a {@link java.lang.Exception} object.
     * @return a {@link java.util.Optional} object.
     */
    public Optional<IExceptionHandler<Exception, ?>> matchBestExceptionHandler(Exception ex) {
        @SuppressWarnings("unchecked")
        List<IExceptionHandler<Exception, ?>> exceptionHandlers = applicationContext.getBeansOfType(IExceptionHandler.class)
                .values().stream().map(t -> (IExceptionHandler<Exception, ?>) t).collect(Collectors.toList());

        final List<IExceptionHandler<Exception, ?>> matchExceptionHandlers = exceptionHandlers
                .stream()
                .filter(t -> ClassUtil.isAssignable(t.getExceptionClass(), ex.getClass()))
                .collect(Collectors.toList());
        if (!matchExceptionHandlers.isEmpty()) {
            log.info("exception [{}] find ExceptionHandlers {}", ex.getClass().getSimpleName(), matchExceptionHandlers.stream().map(AopUtils::getTargetImplClass).map(Class::getSimpleName).collect(Collectors.toList()));
            matchExceptionHandlers.sort(Comparator.comparingInt(eh -> {
                return matchNumber((Class<Exception>) ex.getClass(), eh.getExceptionClass(), 0);
            }));
            IExceptionHandler<Exception, ?> bestMatchExceptionHandler = CollUtil.getFirst(matchExceptionHandlers);
            log.info("exception [{}] best match ExceptionHandler is [{}]", ex.getClass().getSimpleName(), AopUtils.getTargetImplClass(bestMatchExceptionHandler).getSimpleName());
            return Optional.of(bestMatchExceptionHandler);
        }
        return Optional.empty();
    }

    /**
     * 计算异常匹配分数
     * @param targetClass 发生的异常类
     * @param sourceClass IExceptionHandler处理的异常类
     * @return 值越小表示越接近，0表示相等
     */
    private <S extends Exception, T extends S> int matchNumber(@NotNull Class<S> targetClass, @NotNull Class<T> sourceClass, @Min(0) int number) {
//        Class<?> targetClass = AopUtils.getTargetImplClass(ex);
        //如果直接相等
        if (targetClass.equals(sourceClass)) {
            return number;
        }
        //如果没有了父类
        //noinspection unchecked
        Class<S> superClass = (Class<S>) targetClass.getSuperclass();
        if (Object.class.equals(superClass)) {
            return Integer.MAX_VALUE;
        }
        return matchNumber(superClass, sourceClass, number + 1);
    }

}
