package com.zhituanbox.web.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;
import com.zhituanbox.core.config.BoxCoreConstant;
import com.zhituanbox.core.console.Console;
import com.zhituanbox.core.console.RowMapConsole;
import com.zhituanbox.core.lambda.util.LambdaUtils;
import com.zhituanbox.web.controller.config.CorsProperties;
import com.zhituanbox.web.controller.config.RestWrapperResponseBodyProperties;
import com.zhituanbox.web.controller.core.response.AbstractRestWrapperResponseBodyAdvice;
import com.zhituanbox.web.controller.core.response.RestWrapperResponseBodyAdvice;
import com.zhituanbox.web.controller.core.response.RestWrapperResponseBodyHandler;
import com.zhituanbox.web.controller.support.response.DefaultRestWrapperResponseBodyHandler;
import com.zhituanbox.web.controller.support.response.ResponseData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.web.servlet.mvc.method.annotation.ExceptionHandlerExceptionResolver;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>RestWrapperResponseBodyAutoConfiguration class.</p>
 *
 * @author LiuYang
 * @since 2020/9/30
 * @version 1.0.0
 */
@Configuration(proxyBeanMethods = false)
@EnableConfigurationProperties
@ConditionalOnProperty(prefix = RestWrapperResponseBodyProperties.PROPERTIES_PREFIX, value = BoxCoreConstant.ENABLE_PROPERTIES_KEY, havingValue = "true")
@AutoConfigureOrder(Ordered.HIGHEST_PRECEDENCE)
@AutoConfigureBefore({WebMvcAutoConfiguration.class, ExceptionHandlerExceptionResolver.class})
public class RestWrapperResponseBodyAutoConfiguration {

    private final Logger log = LoggerFactory.getLogger(RestWrapperResponseBodyAutoConfiguration.class);

    /**
     * <p>restWrapperResponseBodyProperties.</p>
     *
     * @return a {@link com.zhituanbox.web.controller.config.RestWrapperResponseBodyProperties} object.
     */
    @Bean
    @ConfigurationProperties(prefix = RestWrapperResponseBodyProperties.PROPERTIES_PREFIX)
    public RestWrapperResponseBodyProperties restWrapperResponseBodyProperties() {
        return new RestWrapperResponseBodyProperties();
    }

    /**
     * <p>restWrapperResponseBodyAdvice.</p>
     *
     * @param restWrapperResponseBodyProperties a {@link com.zhituanbox.web.controller.config.RestWrapperResponseBodyProperties} object.
     * @return a {@link com.zhituanbox.web.controller.core.response.AbstractRestWrapperResponseBodyAdvice} object.
     */
    @Bean("restWrapperResponseBodyAdvice")
    @ConditionalOnMissingBean
    public AbstractRestWrapperResponseBodyAdvice restWrapperResponseBodyAdvice(RestWrapperResponseBodyProperties restWrapperResponseBodyProperties) {
        logTable(restWrapperResponseBodyProperties, RestWrapperResponseBodyAdvice.class);
        return new RestWrapperResponseBodyAdvice();
    }

    /**
     * <p>restWrapperResponseBodyHandler.</p>
     *
     * @return a {@link com.zhituanbox.web.controller.core.response.RestWrapperResponseBodyHandler} object.
     */
    @Bean
    @ConditionalOnMissingBean
    public RestWrapperResponseBodyHandler<ResponseData<?>> restWrapperResponseBodyHandler() {
        return new DefaultRestWrapperResponseBodyHandler();
    }

    private void logTable(@NotNull RestWrapperResponseBodyProperties restWrapperResponseBodyProperties, Class<?> clazz) {
        final String header = StrUtil.format("全局返回值处理: {}.class", clazz.getSimpleName());
        Map<String, String> items = new LinkedHashMap<>();
        Map<String, Object> bean = BeanUtil.beanToMap(restWrapperResponseBodyProperties, false, true);

        @NotBlank String resolveProperty = LambdaUtils.resolveProperty(RestWrapperResponseBodyProperties::getExcludeClasses);
        Set<Class<?>> classes = Optional.ofNullable(restWrapperResponseBodyProperties.getExcludeClasses())
                .map(CollUtil::newLinkedHashSet).orElseGet(LinkedHashSet::new);
        bean.put(resolveProperty, classes);

        bean.forEach((k, v) -> items.put(k, Convert.toStr(v)));
        Console rowConsole = new RowMapConsole(header, items);
        rowConsole.lineHandler(log::info);
    }

}
