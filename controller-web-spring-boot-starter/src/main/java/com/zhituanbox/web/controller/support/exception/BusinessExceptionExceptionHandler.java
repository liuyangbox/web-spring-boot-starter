package com.zhituanbox.web.controller.support.exception;

import com.zhituanbox.core.exception.BusinessException;
import com.zhituanbox.web.controller.core.execption.AbstractExceptionHandler;
import com.zhituanbox.web.controller.core.execption.IExceptionHandler;
import com.zhituanbox.web.controller.support.response.ExceptionResponseData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.i18n.LocaleContext;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * 业务异常
 *
 * @author LiuYang
 * @version 1.0.0
 */
@Order(Ordered.HIGHEST_PRECEDENCE + 10)
public class BusinessExceptionExceptionHandler extends AbstractExceptionHandler<BusinessException> {
    private final Logger log = LoggerFactory.getLogger(BusinessExceptionExceptionHandler.class);

    /** {@inheritDoc} */
    @Override
    public @NotNull Class<BusinessException> getExceptionClass() {
        return BusinessException.class;
    }

    /** {@inheritDoc} */
    @Override
    public ExceptionResponseData<?> processException(@NotNull HttpServletRequest request, @NotNull HttpServletResponse response, @NotNull BusinessException exception) {
        ExceptionResponseData<List<String>> exceptionResponseData = processException(exception, exception.getCode(), "业务异常", exception.getCodeArgs());
        exceptionResponseData.setBusinessException(true);
        log(log, exception, exceptionResponseData);
        return exceptionResponseData;
    }
}
